lagrange :: [(Double, Double)] -> Double -> Double
lagrange xs x = foldl (\acc (xj, y) -> acc + (y * l xj)) 0 xs
                where l xj = foldl (\acc (xm,_) -> if xm == xj then acc else acc * ((x-xm)/(xj-xm))) 1 xs