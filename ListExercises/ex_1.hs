isIn :: (Eq a) => a -> [a] -> Bool
isIn _ []  = False
isIn el (x:xs) = (el == x) || (isIn el xs)