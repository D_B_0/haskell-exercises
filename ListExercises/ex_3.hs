isAsc :: [Int] -> Bool
isAsc [] = True
isAsc [x] = True
isAsc (x:y:xs)
    | x <= y     = isAsc $ y : xs
    | otherwise  = False