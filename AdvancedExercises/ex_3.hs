import System.IO
import Data.Char

-- readFile :: String -> IO String
-- putStr :: String -> IO ()
-- putStrLn :: String -> IO ()
-- getLine :: IO String

getSearchWords :: IO [String]
getSearchWords = do
    putStrLn "Words to search:"
    aux
    where
      aux = do
        putStr "> "
        line <- getLine
        if line == "" then
          return []
        else do
          xs <- aux
          return $ line : xs

lower :: String -> String
lower = map toLower

findStrings :: [String] -> String -> [String]
findStrings strings text = [w | w <- strings, (lower w) `elem` txtwords]
                         where
                           txtwords = map lower $ words $ filter (\x -> isLetter x || isSpace x) text

main :: IO ()
main = do
       hSetBuffering stdout NoBuffering
       words <- getSearchWords
       putStr "File to search: "
       path <- getLine
       text <- readFile path
       let foundWords = findStrings words text
       let notFoundWords = filter (\w -> not $ w `elem` foundWords) words
       -- My solution
       -- putStr $ foldr (\w acc -> acc ++ "\"" ++ w ++ "\"\tfound\n") "" foundWords
       -- putStr $ foldr (\w acc -> acc ++ "\"" ++ w ++ "\"\tnot found\n") "" notFoundWords
       -- Tutorial solution
       mapM_ (\w -> putStrLn $ "\"" ++ w ++ "\"\tfound") foundWords 
       mapM_ (\w -> putStrLn $ "\"" ++ w ++ "\"\tnot found") notFoundWords 
