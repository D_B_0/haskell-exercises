data Tree a = Leaf | Node (Tree a) a (Tree a)

instance (Show a) => Show (Tree a)
         where
         show (Leaf) = "Leaf"
         show (Node l v r) = "Node (" ++ (show l) ++ " " ++ (show v) ++ " " ++ (show r) ++ ")"

insert :: (Ord a) => a -> Tree a -> Tree a
insert v Leaf = Node Leaf v Leaf
insert v (Node l vt r)
     | v <= vt = Node (insert v l) vt r
     | v >  vt = Node l vt (insert v r)


inorder :: (Ord a) => Tree a -> [a] 
inorder Leaf = []
inorder (Node l v r) = (inorder l) ++ [v] ++ (inorder r)
